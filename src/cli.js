
const { readConfig, writeToConfig } = require('./utils/config');
const artifactoryUploader = require('./artifactoryUploader');
const program = require('commander');

function setup() {
    program
        .version('1.0.0', '-v, --version');

    program
        .command('folder <folderPath> <repositoryName>')
        .description("Upload the folder's content to the repository")
        .action(artifactoryUploader.uploadFolderContentToRepository);

    program
        .command('multi <folderPath>')
        .description('Upload to multiple repositories at once')
        .action(artifactoryUploader.uploadFolderContentToMultipleRepositories);

    program
        .command('get')
        .option('-r, --registry', "Returns the current working registry",
            () => { console.log(readConfig().artifactoryConnectionString) })
        .action(() => { });

    program
        .command('set')
        .option('-r, --registry <registryUrl>', "Sets the current working registry",
            (registryUrl) => { writeToConfig({ artifactoryConnectionString: registryUrl }) })
        .action(() => { });

    program
        .command('*', { noHelp: true })
        .action(() => { program.help() });

    program.parse(process.argv);

    if (!program.args.length)
        program.help();
}

exports.setup = setup;