
const artifactoryUploader = require('./artifactoryUploader');
const cli = require('./cli');

cli.setup();


exports.uploadFile = artifactoryUploader.uploadFile;
exports.uploadFolderContentToRepository = artifactoryUploader.uploadFolderContentToRepository;
exports.uploadFolderContentToMultipleRepositories = artifactoryUploader.uploadFolderContentToMultipleRipositories;