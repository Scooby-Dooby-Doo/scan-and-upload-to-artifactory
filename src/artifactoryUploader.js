
const { readConfig } = require('./utils/config');
const fs = require('fs');
const request = require('request');
const utils = require('./utils/utils');


function uploadFile(filePath, url, successResponseEventHandler, errorEventHandler, errorResponseEventHandler) {
    let stream = fs.createReadStream(filePath);
    stream.pipe(request
        .put(url, {}, (error, request, body) => {
            if (error)
                errorEventHandler(error)
        })
        .on('response', function (response) {
            if (utils.isSuccessStatusCode(response.statusCode))
                successResponseEventHandler();
            else
                errorResponseEventHandler(response);
        })
    );
}

function uploadFolderContentToMultipleRepositories(folderPath) {
    try {
        const repositoriesNames = utils.getDirectories(folderPath);

        repositoriesNames.forEach(repositoryName => {
            uploadFolderContentToRepository(repositoryName, folderPath);
        });

    } catch (error) {
        console.error(error);
    }
}

function uploadFolderContentToRepository(folderPath, repositoryName) {
    try {
        const { artifactoryConnectionString } = readConfig();

        if (!artifactoryConnectionString) {
            console.error('Please set the Artifactory url before uploading.');
            return;
        }

        const files = utils.walkSync(folderPath);
        files.forEach(filePath => {
            uploadFile(
                filePath,
                utils.buildFullArtifactUrl(artifactoryConnectionString, repositoryName, folderPath, filePath),
                successResponseEventHandler(folderPath, filePath),
                errorEventHandler(folderPath, filePath),
                errorResponseEventHandler(folderPath, filePath)
            );
        });

    } catch (error) {
        console.error(error);
    }
}

function successResponseEventHandler(folderPath, filePath) {
    return () => {
        console.log('Uploaded the file /' +
            utils.buildArtifactPath(folderPath, filePath))
    };
}

function errorEventHandler(folderPath, filePath) {
    return (error) => {
        console.error("Couldn't upload the file " +
            utils.buildArtifactPath(folderPath, filePath) + 
            '  (' + error.message + ')');
    };
}

function errorResponseEventHandler(folderPath, filePath) {
    return (response) => {
        console.error("Couldn't upload the file " +
            utils.buildArtifactPath(folderPath, filePath) + 
            "  (status code: " + response.statusCode + ")");
    };
}

exports.uploadFile = uploadFile;
exports.uploadFolderContentToRepository = uploadFolderContentToRepository;
exports.uploadFolderContentToMultipleRepositories = uploadFolderContentToMultipleRepositories;