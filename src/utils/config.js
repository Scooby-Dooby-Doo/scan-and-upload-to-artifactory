
const fs = require('fs');


const CONFIG_FILE_PATH = './config.json';


function readConfig() {
    if(!fs.existsSync(CONFIG_FILE_PATH)) {
        createEmptyFileSync(CONFIG_FILE_PATH);
    }
    return JSON.parse(fs.readFileSync(CONFIG_FILE_PATH));
}

function writeToConfig(obj) {
    const currentConfig = readConfig();
    const newConfig = Object.assign(currentConfig, obj);

    fs.writeFileSync(CONFIG_FILE_PATH, JSON.stringify(newConfig));
}


function createEmptyFileSync(filePath) {
    fs.closeSync(fs.openSync(filepath, 'w'));
}

exports.readConfig = readConfig;
exports.writeToConfig = writeToConfig;