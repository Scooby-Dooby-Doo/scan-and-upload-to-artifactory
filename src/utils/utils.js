
const { readConfig } = require('./config');
const fs = require('fs');
const urljoin = require('url-join');
const { join } = require('path');


function buildFullArtifactUrl(artifactoryConnectionString, repositoryName, folder, filePath) {
    return urljoin(
        artifactoryConnectionString, 
        readConfig().artifactoryTopDirectory,
        repositoryName, 
        buildArtifactPath(folder, filePath)
    );
}

function buildArtifactPath(folder, filePath) {
    folder = addSlashToStringEndIfNecessary(folder);
    return filePath.substring(folder.length);
}

function walkSync(folder) {
    folder = addSlashToStringEndIfNecessary(folder);
    return _walkSync(folder, null);
}

function _walkSync(dir, filelist) {
    files = fs.readdirSync(dir);
    filelist = filelist || [];
    files.forEach(function (file) {
        if (fs.statSync(dir + file).isDirectory()) {
            filelist = _walkSync(dir + file + '/', filelist);
        }
        else {
            filelist.push(dir + file);
        }
    });
    return filelist;
}

function getDirectories(source) {
    const names = fs.readdirSync(source);

    return names.filter(name => {
        return isDirectory(join(source, name));  //I could have curried
    });
}

function isDirectory(source) {
    return fs.lstatSync(source).isDirectory();
}

function isSuccessStatusCode(statusCode) {
    return statusCode >= 200 && statusCode <=299;
}

function addSlashToStringEndIfNecessary(str) {
    return str.endsWith('/') ? str : str + '/';
}

exports.walkSync = walkSync;
exports.buildFullArtifactUrl = buildFullArtifactUrl;
exports.getDirectories = getDirectories;
exports.buildArtifactPath = buildArtifactPath;
exports.isSuccessStatusCode = isSuccessStatusCode;